package com.edugorilla.ssologin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.webkit.WebViewClient;

public class WebView extends AppCompatActivity {
    private ProgressDialog progress_dialog;
    private android.webkit.WebView web_view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        progress_dialog = ProgressDialog.show(this, "", "Please wait...", true);
        progress_dialog.setCancelable(false);

        String url = getIntent().getStringExtra("url");

        web_view = findViewById(R.id.web_view);
        web_view.getSettings().setJavaScriptEnabled(true);
        web_view.getSettings().setLoadWithOverviewMode(true);
        web_view.getSettings().setUseWideViewPort(true);
        web_view.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(android.webkit.WebView view, final String url) {
                if (progress_dialog.isShowing()) {
                    progress_dialog.dismiss();
                }
            }
        });
        web_view.loadUrl(url);
    }
}